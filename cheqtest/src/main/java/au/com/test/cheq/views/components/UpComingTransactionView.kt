package au.com.test.cheq.views.components

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.layout_single_coming_up_transaction.view.*

/**
 * This class holds an individual upcoming transaction view
 */
class UpComingTransactionView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var textAmount: TextView
    var imageLogo: ImageView
    var textDaysUntil: TextView

    init {
        this.textAmount = itemView.text_amount
        this.imageLogo = itemView.image_logo
        this.textDaysUntil = itemView.text_days_until
    }
}