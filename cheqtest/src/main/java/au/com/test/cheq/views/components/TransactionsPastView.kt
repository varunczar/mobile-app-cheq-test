package au.com.test.cheq.views.components

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import au.com.test.cheq.R
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.utils.formatTransaction
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.layout_single_past_transaction.view.*

class TransactionsPastView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0) : LinearLayout(context, attrs, defStyle) {

    val TAG = "TransactionsPastView"


    init {
        LayoutInflater.from(context)
                .inflate(R.layout.layout_single_past_transaction, this)

        orientation = LinearLayout.HORIZONTAL
    }

    /**
     * This method sets the transactions data
     */
    fun setData(transaction: Transaction?) {

        transaction?.let {
            Log.i(TAG, "Setting transaction data for [$transaction]")
            //Setting the name of the transaction
            text_name.text = it.name
            //Setting the dummy logo
            it.logoUrl?.let { Glide.with(this).load(it).diskCacheStrategy(DiskCacheStrategy.ALL).into(image_logo) }
            //Setting the transaction amount
            text_amount.text = it.formatTransaction()
        }

    }
}