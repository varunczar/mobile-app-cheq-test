package au.com.test.cheq.views.core

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.Toast
import au.com.test.cheq.R
import au.com.test.cheq.contracts.DashboardContract
import au.com.test.cheq.utils.Constants
import au.com.test.cheq.utils.Constants.KEY_MENU
import au.com.test.cheq.utils.clearFragment
import au.com.test.cheq.utils.replaceFragmentSafely
import au.com.test.cheq.views.fragments.EarnFragment
import au.com.test.cheq.views.fragments.ProfileFragment
import au.com.test.cheq.views.fragments.SaveFragment
import au.com.test.cheq.views.fragments.SpendFragment
import kotlinx.android.synthetic.main.layout_custom_action.view.*
import kotlinx.android.synthetic.main.layout_menu.*

/**
 * This is the main entry point of the application that helps switching between different screens as navigated via the menu
 */
class MainActivity : AppCompatActivity(), DashboardContract.DashboardView, BottomNavigationView.OnNavigationItemSelectedListener {

    //This stores the current state of the screen navigation
    var menuItemId = R.id.menu_spend

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Setup action bar
        setUpActionItems()
        //Set this class as the navigation menu listener
        menu_navigation.setOnNavigationItemSelectedListener(this)
        //On screen rotation check if a screen is open and restore the app to the set state
        if (savedInstanceState != null &&
                savedInstanceState.containsKey(KEY_MENU) &&
                savedInstanceState.get(KEY_MENU) != null) {
            menuItemId = savedInstanceState.getInt(KEY_MENU)
            Log.i(TAG, "Displaying the stored state");
            performMenuNavigation(menuItemId)
        } else {
            Log.i(TAG, "Displaying the launch default screen");
            showSpend()
        }

    }

    /**
     * This method sets up the action bar
     */
    private fun setUpActionItems() {
        //Set up the action bar
        val mActionBar = supportActionBar
        mActionBar!!.setDisplayShowHomeEnabled(false)
        mActionBar.setDisplayShowTitleEnabled(false)
        mActionBar.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.colorBackground, null)))

        //Inflate and set the action bar icons
        val mInflater = LayoutInflater.from(this)
        val mCustomView = mInflater.inflate(R.layout.layout_custom_action, null)

        //Set dummy responses for the action bar items
        mCustomView.image_chat.setOnClickListener { Toast.makeText(this, resources.getString(R.string.message_to_do), Toast.LENGTH_SHORT).show() }
        mCustomView.image_search.setOnClickListener { Toast.makeText(this, resources.getString(R.string.message_to_do), Toast.LENGTH_SHORT).show() }
        mCustomView.image_settings.setOnClickListener { Toast.makeText(this, resources.getString(R.string.message_to_do), Toast.LENGTH_SHORT).show() }

        mActionBar.setCustomView(mCustomView)
        mActionBar.setDisplayShowCustomEnabled(true)
        mActionBar.elevation = 0f

    }

    /**
     * This method displays the spend screen
     */
    override fun showSpend() {
        Log.i(TAG, "Displaying the spend screen");
        val spendFragment = SpendFragment()
        this.replaceFragmentSafely(spendFragment, Constants.FRAGMENT, false, R.id.fragment_contents)

    }

    /**
     * This method displays the earn screen
     */
    override fun showEarn() {
        Log.i(TAG, "Displaying the earn screen");
        val earnFragment = EarnFragment()
        this.replaceFragmentSafely(earnFragment, Constants.FRAGMENT, true, R.id.fragment_contents)
    }

    /**
     * This method displays the profile screen
     */
    override fun showProfile() {
        Log.i(TAG, "Displaying the profile screen");
        val profileFragment = ProfileFragment()
        this.replaceFragmentSafely(profileFragment, Constants.FRAGMENT, true, R.id.fragment_contents)
    }

    /**
     * This method displays the save screen
     */
    override fun showSave() {
        Log.i(TAG, "Displaying the save screen");
        val saveFragment = SaveFragment()
        this.replaceFragmentSafely(saveFragment, Constants.FRAGMENT, true, R.id.fragment_contents)
    }

    /**
     * This method is invoked when the bottom navigation items are clicked
     */
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        Log.i(TAG, "Navigation menu clicked");
        //Clear the screen
        clearFragment(Constants.FRAGMENT)
        //Storing the selected menu item
        menuItemId = menuItem.itemId
        //Perform menu navigation
        performMenuNavigation(menuItem.itemId)
        return true

    }

    /**
     * This method displays a particular screen based on the menu item id
     */
    private fun performMenuNavigation(menuItemId: Int) {
        when (menuItemId) {
            R.id.menu_earn -> showEarn()
            R.id.menu_profile -> showProfile()
            R.id.menu_save -> showSave()
            else -> showSpend()
        }
    }

    /**
     * This method stores the current state of the opened screen
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(KEY_MENU, menuItemId);
    }

}
