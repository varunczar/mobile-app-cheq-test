package au.com.test.cheq.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.test.cheq.R
import au.com.test.cheq.data.pojo.AccountSnapshot
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.utils.Constants.ACCOUNT_SNAPSHOT
import au.com.test.cheq.utils.Constants.TRANSACTIONS_COMING_UP
import au.com.test.cheq.utils.Constants.TRANSACTIONS_PAST
import au.com.test.cheq.views.components.AccountSummaryViewHolder
import au.com.test.cheq.views.components.TransactionsComingUpViewHolder
import au.com.test.cheq.views.components.TransactionsPastViewHolder

/**
 * This adapter class is responsible for creating the Spend Screen view items
 */
class RecyclerViewDataAdapter(private val mAccountSnapshot: AccountSnapshot?,
                              private val mComingUpTransactions: ArrayList<Transaction>?,
                              private val mPastTransactions: ArrayList<Transaction>?, private val mContext: Context) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * This method inflates a view based on the type of the view
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
        /* Inflate an account summary view to display account details */
            ACCOUNT_SNAPSHOT -> {
                var accountSummaryViewHolder = inflater.inflate(R.layout.layout_account_snapshot, parent, false)
                viewHolder = AccountSummaryViewHolder(accountSummaryViewHolder)
            }
        /* Inflate a transaction group row for transaction details */
            TRANSACTIONS_COMING_UP -> {
                var transactionViewHolder = inflater.inflate(R.layout.layout_coming_up, parent, false)
                viewHolder = TransactionsComingUpViewHolder(transactionViewHolder)
            }
            else -> {
                var transactionViewHolder = inflater.inflate(R.layout.layout_past_transaction_holder, parent, false)
                viewHolder = TransactionsPastViewHolder(transactionViewHolder)
            }
        }
        return viewHolder
    }

    /**
     * This method binds the view to the data based on the view type
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {

        //Create an account summary view holder
            ACCOUNT_SNAPSHOT -> {
                val viewHolder = holder as AccountSummaryViewHolder
                viewHolder.setData(mAccountSnapshot)

            }

        //Create an upcoming transactions view holder
            TRANSACTIONS_COMING_UP -> {
                val viewHolder = holder as TransactionsComingUpViewHolder
                viewHolder.setData(mComingUpTransactions, mContext)

            }

        //Create a past transactions view holder
            TRANSACTIONS_PAST -> {
                val viewHolder = holder as TransactionsPastViewHolder
                val index = position - 2
                viewHolder.setData(mPastTransactions?.get(index), index == 0)

            }

        }


    }

    /**
     * This method returns the number of items
     */
    override fun getItemCount(): Int {
        if (mPastTransactions?.isNotEmpty()!!
                && mComingUpTransactions?.isNotEmpty()!!) {
            return 2 + mPastTransactions.size
        } else {
            return 0;
        }
    }


    /* Returns the view type based on the instance type in a list of heterogenous objects */
    override fun getItemViewType(position: Int): Int {
        when (position) {
            0 -> return ACCOUNT_SNAPSHOT
            1 -> return TRANSACTIONS_COMING_UP
            else -> return TRANSACTIONS_PAST
        }
    }
}