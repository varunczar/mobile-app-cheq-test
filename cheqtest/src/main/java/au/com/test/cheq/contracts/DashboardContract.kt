package au.com.test.cheq.contracts

/**
 * This is a Dashboard contract class that houses the view contracts for the Main Dashboard
 */
interface DashboardContract {

    interface DashboardView {
        fun showSpend()
        fun showSave()
        fun showEarn()
        fun showProfile()
    }
}