package au.com.test.cheq.data.network

import com.squareup.okhttp.OkHttpClient
import retrofit.JacksonConverterFactory
import retrofit.Retrofit

/**
 * This class creates a singleton retrofit client instance to connect to the Account URL
 */
object RestClient {

    private var mRestService: AccountDetailsService? = null

    val client: AccountDetailsService
        get() {
            if (mRestService == null) {
                //Create a retrofit instance if not created before
                val client = OkHttpClient()
                client.interceptors().add(FakeInterceptor())

                val retrofit = Retrofit.Builder()
                        .addConverterFactory(JacksonConverterFactory.create())
                        .baseUrl(AccountDetailsService.BASE_URL)
                        .client(client)
                        .build()

                mRestService = retrofit.create(AccountDetailsService::class.java)
            }
            return mRestService!!
        }
}
