package au.com.test.cheq.views.components

import android.support.v7.widget.RecyclerView
import android.view.View
import au.com.test.cheq.data.pojo.Transaction
import kotlinx.android.synthetic.main.layout_past_transaction_holder.view.*

/**
 * This class is a viewholder that houses the past transactions view
 */
class TransactionsPastViewHolder(val mItemView: View) : RecyclerView.ViewHolder(mItemView) {

    /*
     * This method sets the transactions data by calling on the Past Transactions view
     */
    fun setData(transaction: Transaction?, showTitle: Boolean) {
        if (showTitle) mItemView.text_title_today.visibility = View.VISIBLE
        else mItemView.text_title_today.visibility = View.GONE
        mItemView.view_past_transaction.setData(transaction)
    }
}