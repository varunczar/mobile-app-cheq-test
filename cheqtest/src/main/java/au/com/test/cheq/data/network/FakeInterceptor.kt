package au.com.test.cheq.data.network

import android.util.Log
import au.com.test.cheq.utils.Constants.CLIENT_ID_1
import au.com.test.cheq.utils.Constants.CLIENT_ID_2
import au.com.test.cheq.utils.Utils
import com.squareup.okhttp.*
import java.io.IOException

/**
 * This class enables mocking a JSON response that would otherwise be returned by an API
 * It intercepts requests to a server, processes it, and returns a hardcoded response of account information
 */
class FakeInterceptor : Interceptor {

    val TAG = "FakeInterceptor"

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        var response: Response
        var responseString = ""
        // Build a uri
        val uri = chain.request().uri()
        val query = uri.getQuery()
        val splitParameters = query.split("=".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
        if (splitParameters[1] == CLIENT_ID_1) {
            Log.i(TAG, "Fetching response for account with client id = 1")
            responseString = Utils.getSuccessfulResponse()
        } else if (splitParameters[1] == CLIENT_ID_2) {
            Log.i(TAG, "Fetching response for account with client id = 2")
            responseString = Utils.getEmptyResponse()
        }
        //Build the response with the returned json string
        response = Response.Builder()
                .code(200)
                .message(responseString)
                .request(chain.request())
                .protocol(Protocol.HTTP_2)
                .body(ResponseBody.create(MediaType.parse("application/json"), responseString.toByteArray()))
                .addHeader("content-type", "application/json")
                .build()
        Log.i(TAG, "Sending response back to the client")
        return response
    }
}