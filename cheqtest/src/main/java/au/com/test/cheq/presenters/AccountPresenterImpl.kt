package au.com.test.cheq.presenters

import android.content.Context
import android.util.Log
import au.com.test.cheq.R
import au.com.test.cheq.contracts.AccountContract
import au.com.test.cheq.data.network.NetworkUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete

/**
 * This presenter class takes in requests from the account view, contacts the network layer for data,
 * and with the results returned, passes them back to the view to render as implemented
 */
class AccountPresenterImpl(val mAccountView: AccountContract.AccountView, val mContext: Context) : AccountContract.AccountPresenter {

    val TAG = "AccountPresenterImpl"

    override fun fetchAccountSnapshotForClient(clientId: String) {

        //Show progress bar
        Log.i(TAG, "Displaying progress bar")
        mAccountView.showProgress()
        doAsync {

            //Fetch account info for client
            Log.i(TAG, "Fetching account info for client [$clientId] from the network layer")
            val apiResponse = NetworkUtils.getAccountSnapshot(clientId)

            onComplete {
                Log.i(TAG, "Request processed")
                //Hide progress bar
                Log.i(TAG, "Hiding progress bar")
                mAccountView.hideProgress()
                if (apiResponse?.accountSnapshot == null &&
                        apiResponse?.comingUpTransactions?.isEmpty()!! &&
                        apiResponse.transactions?.isEmpty()!!) {
                    Log.i(TAG, "No transactions returned")
                    //Transactions are empty
                    mAccountView.showErrorMessage(mContext.resources.getString(R.string.message_no_data))
                } else {
                    Log.i(TAG, "Setting account data")
                    //Sending results back to the view for rendering
                    mAccountView.setAccountData(apiResponse.accountSnapshot,
                            apiResponse.comingUpTransactions,
                            apiResponse.transactions)
                }
            }

        }
    }
}