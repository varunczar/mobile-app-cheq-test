package au.com.test.cheq.utils

import android.graphics.Paint
import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.TypefaceSpan


/**
 * This utlity class enables one textview to have more than one font typeface
 */
class CustomFont(family: String, private val typeFace: Typeface) : TypefaceSpan(family) {

    override fun updateDrawState(ds: TextPaint) {
        applyCustomTypeFace(ds, typeFace)
    }

    override fun updateMeasureState(paint: TextPaint) {
        applyCustomTypeFace(paint, typeFace)
    }

    private fun applyCustomTypeFace(paint: Paint, typeface: Typeface) {
        val oldStyle: Int
        //Get the old typeface
        val old = paint.getTypeface()
        if (old == null) {
            oldStyle = 0
        } else {
            oldStyle = old.getStyle()
        }
        //Determine the typeface to be overridden
        val style = oldStyle and typeface.style.inv()
        if (style and Typeface.BOLD != 0) {
            paint.setFakeBoldText(true)
        }
        //Set the typeface
        paint.setTypeface(typeface)
    }
}