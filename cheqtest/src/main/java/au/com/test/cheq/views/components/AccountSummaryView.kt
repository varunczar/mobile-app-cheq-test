package au.com.test.cheq.views.components

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import au.com.test.cheq.R
import au.com.test.cheq.data.pojo.AccountSnapshot
import au.com.test.cheq.utils.Constants.SYMBOL_HASH
import au.com.test.cheq.utils.formatDaysToPay
import au.com.test.cheq.utils.formatMoney
import kotlinx.android.synthetic.main.layout_account_summary.view.*


/**
 * This class is responsible for displaying the Account snapshot for a user
 */
class AccountSummaryView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0) : LinearLayout(context, attrs, defStyle) {

    val TAG = "AccountSummaryView"


    init {
        LayoutInflater.from(context)
                .inflate(R.layout.layout_account_summary, this)

        orientation = LinearLayout.VERTICAL
    }

    /**
     * This method sets the account snapshot data
     */
    fun setData(accountSnapshot: AccountSnapshot?) {

        accountSnapshot?.let {
            Log.i(TAG, "Setting accountSnapshot [$accountSnapshot]")
            //Set the amount that is ok to spend
            text_ok_to_spend.text = it.spendingMoney?.formatMoney()
            //Set the amount that is ok to spend per day
            text_about_per_day.text = resources.getString(R.string.title_about_per_day).replace(SYMBOL_HASH, it.spendingMoneyPerDay?.formatMoney()!!)
            //Set the number of days till payday
            text_payday.text = resources.getString(R.string.title_payday).formatDaysToPay(it.daysToPayDay, context)
        }
    }

}