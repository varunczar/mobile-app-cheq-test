package au.com.test.cheq.utils

/*
 * This utility class houses the hardcoded json responses for client accounts
 */
object Utils {

    /**
     * This method returns a successful response
     */
    fun getSuccessfulResponse(): String {
        return "{ \"account_snapshot\": {    \"spending_money\": \"458.76\",    \"spending_money_per_day\": \"153\",    \"days_to_pay_day\": \"3\"  }, \"coming_up_transactions\": [    {      \"id\": \"1\",      \"date\": \"2019-07-01\",      \"amount\": \"-99.33\",      \"type\": \"D\",      \"name\": \"AGL\", \"logo_url\": \"https://varunczar.com/samplelogo.png\"    },    {      \"id\": \"2\",      \"date\": \"2019-07-03\",      \"amount\": \"-400\",      \"type\": \"D\",      \"name\": \"Personal Loan\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"   },    {      \"id\": \"3\",      \"date\": \"2019-07-03\",      \"amount\": \"-300.3\",      \"type\": \"D\",      \"name\": \"Car Insurance\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"   },    {      \"id\": \"4\",      \"date\": \"2019-07-04\",      \"amount\": \"-900\",      \"type\": \"D\",      \"name\": \"Fitness First\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"    }, {      \"id\": \"5\",      \"date\": \"2019-07-04\",      \"amount\": \"-1000\",      \"type\": \"D\",      \"name\": \"RMS Rego\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"    }  ], \"transactions\": [    {      \"id\": \"1\",      \"date\": \"2019-06-20\",      \"amount\": \"-67.33\",      \"type\": \"D\",      \"name\": \"Woolworths\", \"logo_url\": \"https://varunczar.com/samplelogo.png\"    },    {      \"id\": \"2\",      \"date\": \"2019-06-21\",      \"amount\": \"-100.24\",      \"type\": \"D\",      \"name\": \"Coles\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"   },    {      \"id\": \"3\",      \"date\": \"2019-06-21\",      \"amount\": \"-252.3\",      \"type\": \"D\",      \"name\": \"JB Hifi\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"   },    {      \"id\": \"4\",      \"date\": \"2019-06-21\",      \"amount\": \"500\",      \"type\": \"C\",      \"name\": \"ATM\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"    } ,    {      \"id\": \"5\",      \"date\": \"2019-06-21\",      \"amount\": \"64.9\",      \"type\": \"D\",      \"name\": \"SuperCheap Auto\" , \"logo_url\": \"https://varunczar.com/samplelogo.png\"    }  ]}"
    }

    /**
     * This method returns a successful response with an empty value for transactions
     */
    fun getEmptyResponse(): String {
        return "{ \"account_snapshot\": {    \"spending_money\": \"458.76\",    \"spending_money_per_day\": \"153\",    \"days_to_pay_day\": \"3\"  }, \"coming_up_transactions\": [],  \"transactions\": [ ]}"
    }

}