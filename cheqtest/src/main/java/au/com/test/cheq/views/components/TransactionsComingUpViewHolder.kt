package au.com.test.cheq.views.components

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.utils.formatTotalComingUpDebitTransaction
import au.com.test.cheq.views.adapters.UpComingTransactionsAdapter
import kotlinx.android.synthetic.main.layout_coming_up.view.*

/**
 * This class is a viewholder that houses the upcoming transactions view
 */
class TransactionsComingUpViewHolder(val mItemView: View) : RecyclerView.ViewHolder(mItemView) {

    //Create a view pool to reduce the number of view inflations for single type views
    val recycledViewPool: RecyclerView.RecycledViewPool

    init {
        this.recycledViewPool = RecyclerView.RecycledViewPool()

    }

    /*
     * This method sets the upcoming transactions data
     */
    fun setData(comingUpTransactions: List<Transaction>?, context: Context) {
        val transactions = comingUpTransactions as? ArrayList<Transaction>
        if (transactions?.isNotEmpty()!!) {
            val adapter = UpComingTransactionsAdapter(transactions, context)
            //Set the total with the sum of the upcoming transactions
            mItemView.text_coming_up_total.text = transactions.sumByDouble { it.amount!! }.formatTotalComingUpDebitTransaction()
            //Set up the horizontal recycler view
            mItemView.recycler_view_transactions_upcoming.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            mItemView.recycler_view_transactions_upcoming.adapter = adapter
            mItemView.recycler_view_transactions_upcoming.setRecycledViewPool(recycledViewPool)
            //Hide the error message for no data
            mItemView.text_error.visibility = View.GONE
        } else {
            //Show the error message for no data
            mItemView.text_error.visibility = View.VISIBLE
        }

    }
}