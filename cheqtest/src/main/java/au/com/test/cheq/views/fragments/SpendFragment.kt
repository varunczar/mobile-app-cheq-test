package au.com.test.cheq.views.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import au.com.test.cheq.R
import au.com.test.cheq.contracts.AccountContract
import au.com.test.cheq.data.pojo.AccountSnapshot
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.presenters.AccountPresenterImpl
import au.com.test.cheq.utils.Constants.CLIENT_ID_1
import au.com.test.cheq.views.adapters.RecyclerViewDataAdapter
import kotlinx.android.synthetic.main.fragment_spend.*


/**
 * This fragment class houses the Spend menu item that displays Account Summary, Coming up transactions,
 * and Past transactions
 */
class SpendFragment : Fragment(), AccountContract.AccountView {

    val TAG = "SpendFragment"

    var accountPresenter: AccountContract.AccountPresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_spend, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {
            accountPresenter = AccountPresenterImpl(this, requireContext())
            Log.i(TAG, "Calling on the presenter for client info [$CLIENT_ID_1]")
            //Setting a constant of 1 here as the client ID. Ideally this will be fetched from the backend after the client logs in
            accountPresenter?.fetchAccountSnapshotForClient(CLIENT_ID_1)
            val adapter = RecyclerViewDataAdapter(null, arrayListOf(), arrayListOf(), requireContext())
            recycler_view_spend.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            recycler_view_spend.adapter = adapter
        }

    }

    /**
     * This method renders the spend screen with the data passed in by the presenter
     */
    override fun setAccountData(accountSnapshot: AccountSnapshot?,
                                comingUpTransactions: List<Transaction>?,
                                transactions: List<Transaction>?) {
        context?.let {
            Log.i(TAG, "Setting account data")
            val adapter = RecyclerViewDataAdapter(accountSnapshot,
                    comingUpTransactions as? ArrayList<Transaction>,
                    transactions as? ArrayList<Transaction>, requireContext())
            recycler_view_spend.adapter = adapter
        }
    }

    /**
     * This method shows the progress bar
     */
    override fun showProgress() {
        Log.i(TAG, "Showing progress bar")
        progress_bar?.visibility = View.VISIBLE
    }

    /**
     * This method hides the progress bar
     */
    override fun hideProgress() {
        Log.i(TAG, "Hiding progress bar")
        progress_bar?.visibility = View.GONE
    }

    /**
     * This method shows an error messages
     */
    override fun showErrorMessage(errorMessage: String) {
        Log.i(TAG, "Displaying error message [$errorMessage]")
        context?.let { Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show() }
    }


}
