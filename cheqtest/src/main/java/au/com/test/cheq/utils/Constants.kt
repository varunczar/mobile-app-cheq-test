package au.com.test.cheq.utils

/**
 * This class hosts all constants used across the application
 */
object Constants {

    //View types
    val ACCOUNT_SNAPSHOT = 0
    val TRANSACTIONS_COMING_UP = 1
    val TRANSACTIONS_PAST = 2

    //Common symbols
    val SYMBOL_GREATER_THAN = " >"
    val SYMBOL_DOLLAR = "$"
    val SYMBOL_HASH = "#"
    val SYMBOL_NEGATIVE = "-"
    val SYMBOL_POSITIVE = "+"

    //Generic Fragment Tag to determine an active fragment in the container
    val FRAGMENT = "fragment"

    //Date format of dates returned in the account response
    val DATE_FORMAT = "yyyy-MM-dd"

    //Transaction type - credit
    val TRANSACTION_CREDIT = "C"

    //Key to determine the active menu item in the bottom navigation bar
    val KEY_MENU = "menu_id"

    //Pre-determined client ids
    val CLIENT_ID_1 = "1"
    val CLIENT_ID_2 = "2"
}