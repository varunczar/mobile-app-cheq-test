package au.com.test.cheq.contracts

import au.com.test.cheq.data.pojo.AccountSnapshot
import au.com.test.cheq.data.pojo.Transaction

/**
 * This is an Account contract class that houses the presenter and view contracts for the Account Data
 */
interface AccountContract {

    interface AccountView : BaseContract.BaseView {
        fun setAccountData(accountSnapshot: AccountSnapshot?,
                           comingUpTransactions: List<Transaction>?,
                           transactions: List<Transaction>?)
    }

    interface AccountPresenter {
        fun fetchAccountSnapshotForClient(clientId: String)
    }
}