package au.com.test.cheq.data.network

import au.com.test.cheq.data.pojo.ApiResponse
import retrofit.Call
import retrofit.http.GET
import retrofit.http.Query

/**
 * This service interface sets a contract for Network utility classes to implement
 */
interface AccountDetailsService {

    @GET("/")
    fun getAccountInfoForClient(@Query("client_id") clientId: String): Call<ApiResponse>

    companion object {

        val BASE_URL = "http://www.varunczar.com/"
    }
}