package au.com.test.cheq.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

/**
 * This is a data class for the API Response returned
 */
data class ApiResponse(
        @JsonProperty("account_snapshot")
        val accountSnapshot: AccountSnapshot? = null,
        @JsonProperty("coming_up_transactions")
        @JsonDeserialize(`as` = ArrayList::class, contentAs = Transaction::class)
        val comingUpTransactions: List<Transaction>? = arrayListOf(),
        @JsonProperty("transactions")
        @JsonDeserialize(`as` = ArrayList::class, contentAs = Transaction::class)
        val transactions: List<Transaction>? = arrayListOf()
)