package au.com.test.cheq.contracts

/**
 * This is a Base contract class that houses the baseview to implement common functionalities
 */
interface BaseContract {

    interface BaseView {
        fun showProgress()
        fun hideProgress()
        fun showErrorMessage(errorMessage: String)
    }
}