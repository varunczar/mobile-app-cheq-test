package au.com.test.cheq.data.network

import android.util.Log
import au.com.test.cheq.data.pojo.ApiResponse

/**
 * This utility class enables connecting to the rest client and fetching a json response for a client account request
 */
object NetworkUtils {

    val TAG = "NetworkUtils"

    /**
     * This method communicates with the api, fetches account info for a client id and returns a response
     */
    fun getAccountSnapshot(clientId: String): ApiResponse? {
        val call = RestClient.client.getAccountInfoForClient(clientId)
        try {
            Log.i(TAG, "Making a request for client id [$clientId]")
            val response = call.execute()
            if (response?.isSuccess!!) {
                //Returning a successful response for client
                Log.i(TAG, "Returning a successful response for client id [$clientId]")
                val apiResponse = response.body()
                return apiResponse
            } else {
                //Returning an unsuccessful response for client
                Log.i(TAG, "Returning an unsuccessful response for client id [$clientId]")
                Log.e(TAG, response.message())
                return null
            }
        } catch (e: Exception) {
            //Returning a server error
            Log.e(TAG, "Server error ${e.fillInStackTrace()}")
            return null
        }
    }
}