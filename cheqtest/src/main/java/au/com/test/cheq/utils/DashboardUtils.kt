package au.com.test.cheq.utils

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.util.Log
import au.com.test.cheq.R
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.utils.Constants.DATE_FORMAT
import au.com.test.cheq.utils.Constants.SYMBOL_DOLLAR
import au.com.test.cheq.utils.Constants.SYMBOL_GREATER_THAN
import au.com.test.cheq.utils.Constants.SYMBOL_NEGATIVE
import au.com.test.cheq.utils.Constants.SYMBOL_POSITIVE
import au.com.test.cheq.utils.Constants.TRANSACTION_CREDIT
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

val formatter = SimpleDateFormat(DATE_FORMAT)

val TAG = "DashboardUtils"

/**
 * This method is responsible for handling fragment transactions, it enables replacement of
 * fragment contents (Spend, Save, Earn and Profile screens
 */
fun AppCompatActivity.replaceFragmentSafely(fragment: Fragment,
                                            tag: String,
                                            allowStateLoss: Boolean = false,
                                            @IdRes containerViewId: Int) {
    val ft = supportFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment, tag)
    if (!supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

/**
 * This method is responsible for clearing fragments from the container
 */
fun AppCompatActivity.clearFragment(tag: String) {
    val fragment = supportFragmentManager.findFragmentByTag(tag)
    if (fragment != null)
        supportFragmentManager.beginTransaction().remove(fragment).commit()
}

/**
 * This method formats a currency by appending a dollar sign to it. It can be further enhanced to
 * support multiple currency values based on the locale set
 */
fun Double.formatMoney(): String {
    return SYMBOL_DOLLAR + this.toString()

}

/**
 * This method enables the "days to pay" textview to have both a light and bold font in the same view
 */
fun String.formatDaysToPay(daysToPay: Int?, context: Context): SpannableStringBuilder {
    val formattedString = this.replace(Constants.SYMBOL_HASH, daysToPay.toString())
    //Create pattern matchers for the first digit in a string
    val pattern = Pattern.compile("^\\D*(\\d)")
    val matcher = pattern.matcher(formattedString)
    matcher.find()
    //Find the index of the first digit
    val index = matcher.start(1)
    val spannableBuilder = SpannableStringBuilder(formattedString)
    //Set a light font to the string contents before the number of days
    spannableBuilder.setSpan(CustomFont("", ResourcesCompat.getFont(context, R.font.montserrat_light)!!), 0, index, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
    //Set a bold font to the string contents starting from the number of days
    spannableBuilder.setSpan(CustomFont("", ResourcesCompat.getFont(context, R.font.montserrat_bold)!!), index, formattedString.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
    return spannableBuilder
}

/**
 * This method formats a transaction value by prefixing the amount with a negative(debit) or
 * positive(credit) sign and also a dollar symbol
 */
fun Transaction.formatTransaction(): String {

    //Transaction if of type credit, prefix a +
    if (TRANSACTION_CREDIT == this.type) {
        return SYMBOL_POSITIVE + SYMBOL_DOLLAR + Math.abs(this.amount!!)
    }
    //Transaction if of type debit, prefix a -
    return SYMBOL_NEGATIVE + SYMBOL_DOLLAR + Math.abs(this.amount!!)
}

/**
 * This method formats a debit transaction value by prefixing the amount with a dollar symbol
 */
fun Double.formatTotalComingUpDebitTransaction(): String {

    return SYMBOL_NEGATIVE + SYMBOL_DOLLAR + Math.abs(this) + SYMBOL_GREATER_THAN
}

/**
 * This method formats an upcoming transaction date, calculates the number of days between today
 * and the upcoming debit date and returns the following based on the difference between the number
 * of days
 * 0 -> Today
 * 1 -> Tomorrow
 * else -> in 'x' days
 */
fun String.formatNumberOfDaysUntil(context: Context): String {

    try {

        val futureDate = Calendar.getInstance()
        val date = formatter.parse(this)
        futureDate.time = date
        val currentDate = Calendar.getInstance()
        val days = TimeUnit.MILLISECONDS.toDays(Math.abs(futureDate.timeInMillis - currentDate.timeInMillis))
        when (days) {
            0L -> return context.getString(R.string.label_today)
            1L -> return context.getString(R.string.label_tomorrow)
            else -> return context.getString(R.string.label_in_x_days).replace("#", days.toString())
        }


    } catch (e: ParseException) {
        Log.e(TAG, "Unable to parse date $this " + e.fillInStackTrace())
        return ""
    }


}