package au.com.test.cheq.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.test.cheq.R
import au.com.test.cheq.data.pojo.Transaction
import au.com.test.cheq.utils.formatNumberOfDaysUntil
import au.com.test.cheq.utils.formatTransaction
import au.com.test.cheq.views.components.UpComingTransactionView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

/**
 * This adapter class is responsible for creating the Coming up transaction view items
 */
class UpComingTransactionsAdapter(private val upComingTransactions: ArrayList<Transaction>?, private val mContext: Context) :
        RecyclerView.Adapter<UpComingTransactionView>() {

    /**
     * This method inflates a card for an individual transaction
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpComingTransactionView {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_single_coming_up_transaction, null)
        return UpComingTransactionView(v)
    }

    /**
     * This method binds individual transaction view to the data
     */
    override fun onBindViewHolder(holder: UpComingTransactionView, position: Int) {
        val itemModel = upComingTransactions!![position]
        //Setting the transaction amount
        holder.textAmount.text = itemModel.formatTransaction()
        //Setting the number of days till the transaction
        holder.textDaysUntil.text = itemModel.date?.formatNumberOfDaysUntil(mContext)
        //Setting the dummy logo
        itemModel.logoUrl?.let {
            Glide.with(holder.imageLogo).load(it).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageLogo)
        }
    }

    /**
     * This method returns the count of the number of items
     */
    override fun getItemCount(): Int {
        return upComingTransactions?.size ?: 0
    }


}