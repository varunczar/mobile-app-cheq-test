package au.com.test.cheq.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * This is a data class for an Account snapshot
 */
data class AccountSnapshot(
        @JsonProperty("spending_money")
        val spendingMoney: Double? = 0.0,
        @JsonProperty("spending_money_per_day")
        val spendingMoneyPerDay: Double? = 0.0,
        @JsonProperty("days_to_pay_day")
        val daysToPayDay: Int? = 0
)