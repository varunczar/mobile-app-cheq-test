package au.com.test.cheq.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty

data class Transaction(
        @JsonProperty("id")
        val id: Long? = null,
        @JsonProperty("date")
        val date: String? = null,
        @JsonProperty("amount")
        val amount: Double? = 0.0,
        @JsonProperty("type")
        val type: String? = "D",
        @JsonProperty("name")
        val name: String? = "",
        @JsonProperty("logo_url")
        val logoUrl: String? = ""

)