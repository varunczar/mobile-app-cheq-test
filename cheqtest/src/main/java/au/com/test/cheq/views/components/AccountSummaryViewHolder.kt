package au.com.test.cheq.views.components

import android.support.v7.widget.RecyclerView
import android.view.View
import au.com.test.cheq.data.pojo.AccountSnapshot
import kotlinx.android.synthetic.main.layout_account_snapshot.view.*

/**
 * This class is a viewholder that houses the account summary view
 */
class AccountSummaryViewHolder(val mItemView: View) : RecyclerView.ViewHolder(mItemView) {

    /*
     * This method sets the account snapshot data by calling on the Account Summary view
     */
    fun setData(accountSnapshot: AccountSnapshot?) {
        mItemView.view_account_summary.setData(accountSnapshot)
    }
}