package au.com.test.cheq.network

import au.com.test.cheq.data.network.NetworkUtils
import org.junit.Assert
import org.junit.Test

class NetworkUtilsTest {

    @Test
    fun getAccountSnapshotTestHappyCase() {
        val apiResponse = NetworkUtils.getAccountSnapshot("1")
        Assert.assertTrue(apiResponse?.transactions?.isNotEmpty()!!)
    }

    @Test
    fun getAccountSnapshotTestEmptyData() {
        val apiResponse = NetworkUtils.getAccountSnapshot("2")
        Assert.assertTrue(apiResponse?.transactions?.isEmpty()!!)
    }

    @Test
    fun getAccountSnapshotTestError() {
        val apiResponse = NetworkUtils.getAccountSnapshot("XYZ")
        Assert.assertNull(apiResponse)
    }
}