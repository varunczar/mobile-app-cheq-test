package au.com.test.cheq.utils

import org.junit.Assert
import org.junit.Test

class UtilsTest {

    @Test
    fun getSuccessfulResponseTest() {
        Assert.assertNotNull(Utils.getSuccessfulResponse())
    }

    @Test
    fun getEmptyResponseTest() {
        Assert.assertNotNull(Utils.getEmptyResponse())
    }
}